module Refinery
  module CardGroups
    class CardGroupsController < ::ApplicationController

      before_action :find_all_card_groups
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @card_group in the line below:
        present(@page)
      end

      def show
        @card_group = CardGroup.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @card_group in the line below:
        present(@page)
      end

    protected

      def find_all_card_groups
        @card_groups = CardGroup.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/card_groups").first
      end

    end
  end
end
