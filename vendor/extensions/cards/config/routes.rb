Refinery::Core::Engine.routes.draw do

  # Frontend routes
  namespace :cards do
    resources :cards, :path => '', :only => [:index, :show]do
      collection do
        post :create
        get :my_own_destroy
      end
      put :update
    end
  end

  # Admin routes
  namespace :cards, :path => '' do
    namespace :admin, :path => Refinery::Core.backend_route do
      resources :cards, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
