module Refinery
  module Cards
    class Card < Refinery::Core::BaseModel
      self.table_name = 'refinery_cards'

      enum card_state_enum: ['created', 'critical', 'expired', 'archived', 'done']

      validates :title, :presence => true, :uniqueness => true

      belongs_to :user, :class_name => '::Refinery::Authentication::Devise::User'
      belongs_to :card_group, :class_name => '::Refinery::CardGroups::CardGroup'
      has_many :developers, :class_name => '::Refinery::Developers::Developer'
      has_many :users, :class_name => '::Refinery::Authentication::Devise::User', :through => :developers

      accepts_nested_attributes_for :developers, reject_if: :all_blank, allow_destroy: true

      # To enable admin searching, add acts_as_indexed on searchable fields, for example:
      #
      #   acts_as_indexed :fields => [:title]

    end
  end
end
