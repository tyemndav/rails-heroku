# This migration comes from refinery_superinquiries (originally 5)
class ChangeStateColumnToInteger < ActiveRecord::Migration[5.1]
  def up
    change_column :refinery_cards, :state, :integer
  end

  def down
    change_column :refinery_cards, :state, :string
  end
end
