# Cards extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/cards
    gem build refinerycms-cards.gemspec
    gem install refinerycms-cards.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-cards.gem
