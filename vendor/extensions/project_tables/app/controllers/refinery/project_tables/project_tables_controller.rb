module Refinery
  module ProjectTables
    class ProjectTablesController < ::ApplicationController

      include ApplicationHelper

      before_action :find_all_project_tables
      before_action :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @project_table in the line below:

        @project_table = Refinery::ProjectTables::ProjectTable.find_by_id(params[params[:project_table_id]]) if params[:project_table_id].present?
        present(@page)
      end

      def show
        @project_table = ProjectTable.find(params[:id])
        get_all_tasks

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @project_table in the line below:
        present(@page)
      end
      
      def get_all_tasks
        @tasks = []
        @grouped_tasks = {}
        @project_table.card_groups.each do | group |
          @tasks += group.cards
          @grouped_tasks[group.title.downcase] = group.cards
        end
      end

      def create
        project_table_params
        project_table_attributes = params[:project_table]
        project_table = Refinery::ProjectTables::ProjectTable.new project_table_attributes
        project_table.save!
        redirect_to refinery.root_path
      end

      def update
        project_table_params
        project_table_attributes = params[:project_table]
        project_table_id = params[:project_table_id]
        project_table = Refinery::ProjectTables::ProjectTable.find_by_id project_table_id
        project_table.update project_table_attributes
        project_table.save!
        redirect_to refinery.project_tables_project_table_path(project_table)
      end

      def my_own_destroy
        project_table_params
        project_table = Refinery::ProjectTables::ProjectTable.find_by_id project_table_params[:id]
        project_table.project_developers.destroy_all
        project_table.destroy!
        redirect_to refinery.root_path
      end


    protected

      def find_all_project_tables
        @project_tables = ProjectTable.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/project_tables").first
      end

     private

      def project_table_params
        params.require(:project_table).permit!#(:title, :color, :user_id)
      end

    end
  end
end
