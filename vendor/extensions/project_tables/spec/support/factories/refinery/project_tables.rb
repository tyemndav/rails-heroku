
FactoryGirl.define do
  factory :project_table, :class => Refinery::ProjectTables::ProjectTable do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

