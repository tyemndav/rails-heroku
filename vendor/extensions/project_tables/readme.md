# Project Tables extension for Refinery CMS.

## How to build this extension as a gem (not required)

    cd vendor/extensions/project_tables
    gem build refinerycms-project_tables.gemspec
    gem install refinerycms-project_tables.gem

    # Sign up for a https://rubygems.org/ account and publish the gem
    gem push refinerycms-project_tables.gem
