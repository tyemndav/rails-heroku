module ApplicationHelper

  def site_bar_visible?
    current_refinery_user.present? && current_refinery_user.has_role?(:refinery)
  end

  def get_day_of_week(date)
    if Date.parse(date).monday?
      return "MON"
    end
    if Date.parse(date).monday?
      return "TUE"
    end
    if Date.parse(date).monday?
      return "WED"
    end
    if Date.parse(date).monday?
      return "THU"
    end
    if Date.parse(date).monday?
      return "FRI"
    end
    if Date.parse(date).monday?
      return "SAT"
    end
    return "SUN"
  end

end
